;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Dropbox REST API, Version 1
;;;
;;; https://www.dropbox.com/developers/reference/api
;;;
;;;  Copyright (C) 2012, Andy Bennett
;;;  All rights reserved.
;;;
;;;  Redistribution and use in source and binary forms, with or without
;;;  modification, are permitted provided that the following conditions are met:
;;;
;;;  Redistributions of source code must retain the above copyright notice, this
;;;  list of conditions and the following disclaimer.
;;;  Redistributions in binary form must reproduce the above copyright notice,
;;;  this list of conditions and the following disclaimer in the documentation
;;;  and/or other materials provided with the distribution.
;;;  Neither the name of the author nor the names of its contributors may be
;;;  used to endorse or promote products derived from this software without
;;;  specific prior written permission.
;;;
;;;  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;;;  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;;;  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;;  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
;;;  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;;;  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;;;  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;;  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
;;;  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;;;  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;;;  POSSIBILITY OF SUCH DAMAGE.
;;;
;;; Andy Bennett <andyjpb@knodium.com>, 2012/10/30
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(module dropbox-lolevel
	(dropbox
	  make-dropbox-app
	  account/info
	  files:get
	  files:put
	  files:post
	  metadata
	  delta
	  revisions
	  restore
	  search
	  shares
	  media
	  copy-ref
	  thumbnails
	  chunked_upload
	  commit_chunked_upload
	  fileops/copy
	  fileops/create_folder
	  fileops/delete
	  fileops/move
	  callback
	  old-output-port)

(import chicken scheme)

(use ports)
(use rest-bind oauth-client intarweb uri-common medea srfi-13 extras)

; Definition of the Dropbox OAuth service.
(define dropbox (make-oauth-service-provider
		  protocol-version: 1.0
		  credential-request-url: "https://api.dropbox.com/1/oauth/request_token"
		  owner-auth-url: "https://www.dropbox.com/1/oauth/authorize"
		  token-request-url: "https://api.dropbox.com/1/oauth/access_token"
		  signature-method: 'plaintext))


(define (make-dropbox-app client-credential)
  (make-oauth-service service: dropbox client-credential: client-credential))


(define callback (make-parameter #f))
(define old-output-port (make-parameter #f))



; /account/info : https://www.dropbox.com/developers/reference/api#account-info
; Description   : Retrieves information about the user's account.
;
(define-method (account/info #!key locale) "https://api.dropbox.com/1/account/info"
	       #f
	       read-json)



; /files (GET) : https://www.dropbox.com/developers/reference/api#files-GET
; Description  : Dowloads a file. Note that this call goes to
;                api-content.dropbox.com instead of api.dropbox.com.
;
;         root : The root relative to which path is specified. Valid values are
;                sandbox and dropbox.
;         path : The path to the file you want to retrieve.
;
; Returns      : The specified file's contents at the requested revision.
;                The HTTP response contains the content metadata in JSON format
;                within an x-dropbox-metadata header.
;
(define-method (files:get root path #!key rev) "https://api-content.dropbox.com/1/files"
	       #f
	       (lambda (in-port) ; Based on copy-port from spiffy-cgi-handlers.
		 (let ((bufsize 65536)
		       (total 0))
		   (let loop ((data (read-string bufsize in-port)))
		     (unless (string-null? data)
		       (display data)
		       (if (callback)
			 (begin
			   (set! total (+ total (string-length data)))
			   (if (port? (old-output-port))
			     (with-output-to-port
			       (old-output-port)
			       (lambda () ((callback) total)))
			     ((callback) total))))
		       (loop (read-string bufsize in-port))))))
	       (lambda (headers) ; read-headers
		 (let ((metadata (header-value 'x-dropbox-metadata headers)))
		   (if metadata
		     (read-json metadata)
		     #f))))



; /files_put  : https://www.dropbox.com/developers/reference/api#files_put
; Description : Uploads a file using PUT semantics. Note that this call goes to
;               api-content.dropbox.com instead of api.dropbox.com.
;
;               This method is in most cases simpler to use than /files (POST).
;               The preferred HTTP method for this call is PUT. For
;               compatibility with browser environments, the POST HTTP method is
;               also recognized.
;
;               Note: Providing a Content-Length header set to the size of the
;               uploaded file is required so that the server can verify that it
;               has received the entire file contents.
;
;               /files_put has a maximum file size limit of 150 MB and does not
;               support uploads with chunked encoding. To upload larger files,
;               use /chunked_upload instead.
;
;        root : The root relative to which path is specified. Valid values are
;               sandbox and dropbox.
;        path : The path to the file you want to retrieve. This parameter should
;               not point to a folder.
;
; Returns     : The metadata for the uploaded file.
;
(define-method (files:put root path #!key locale overwrite parent_rev)
	       (make-request method: 'PUT
			     uri: (uri-reference "https://api-content.dropbox.com/1/files_put/<root>/<path>?param=val"))
	       #f; <>
	       read-json)



; /files (POST) : https://www.dropbox.com/developers/reference/api#files-POST
; Description   : Uploads a file. Note that this call goes to
;                 api-content.dropbox.com instead of api.dropbox.com.
;
;                 We recommend you use /files_put instead due to its simpler
;                 interface.
;
;          root : The root relative to which path is specified. Valid values are
;                 sandbox and dropbox.
;          path : The path to the file you want to retrieve. This parameter
;                 should not point to a folder.
;
;                 /files has a maximum file size limit of 150 MB. To upload
;                 larger files, use /chunked_upload instead.
;
; Returns       : The metadata for the uploaded file.
;
(define-method (files:post root path #!key locale overwrite parent_rev)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api-content.dropbox.com/1/files/<root>/<path>"))
	       #f;<>
	       read-json)



; /metadata   : https://www.dropbox.com/developers/reference/api#metadata
; Description : Retrieves file and folder metadata.
;
;        root : The root relative to which path is specified. Valid values are
;               sandbox and dropbox.
;        path : The path to the file or folder.
;
; Returns     : The metadata for the file or folder at the given <path>. If
;               <path> represents a folder and the list parameter is true, the
;               metadata will also include a listing of metadata for the
;               folder's contents.
;
(define-method (metadata root path #!key file_limit hash list include_deleted rev locale)
	       "https://api.dropbox.com/1/metadata/"
	       #f
	       read-json)



; /delta      : https://www.dropbox.com/developers/reference/api#delta
; Description : A way of letting you keep up with changes to files and folders
;               in a user's Dropbox. You can periodically call /delta to get a
;               list of "delta entries", which are instructions on how to update
;               your local state to match the server's state.
;
; Returns     : A JSON object with four fields
;                 entries, reset, cursor, has_more
(define-method (delta #!key cursor locale)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api.dropbox.com/1/delta"))
	       #f
	       read-json)



; /revisions  : https://www.dropbox.com/developers/reference/api#revisions
; Description : Obtains metadata for the previous revisions of a file.
;
;               Only revisions up to thirty days old are available (or more if
;               the Dropbox user has Pack-Rat). You can use the revision number
;               in conjunction with the /restore call to revert the file to its
;               previous state.
;
;        root : The root relative to which path is specified. Valid values are
;               sandbox and dropbox.
;        path : The path to the file or folder.
;
; Returns     : A list of all revisions formatted just like file metadata.
;
(define-method (revisions root path #!key rev_limit locale)
	       "https://api.dropbox.com/1/revisions/<root>/<path>"
	       #f
	       read-json)



; /restore    : https://www.dropbox.com/developers/reference/api#restore
; Description : Restores a file path to a previous revision.
;
;               Unlike downloading a file at a given revision and then
;               re-uploading it, this call is atomic. It also saves a bunch of
;               bandwidth.
;
;        root : The root relative to which path is specified. Valid values are
;               sandbox and dropbox.
;        path : The path to the file.
;
; Returns     : The metadata of the restored file.
;
(define-method (restore root path #!key rev locale)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api.dropbox.com/1/restore/<root>/<path>"))
	       #f
	       read-json)



; /search     : https://www.dropbox.com/developers/reference/api#search
; Description : Returns metadata for all files and folders whose filename
;               contains the given search string as a substring.
;
;               Searches are limited to the folder path and its sub-folder
;               hierarchy provided in the call.
;
;        root : The root relative to which path is specified. Valid values are
;               sandbox and dropbox.
;        path : The path to the folder you want to search from.
;
; Returns     : List of metadata entries for any matching files and folders.
;
(define-method (search root path #!key query file_limit include_deleted locale)
	       "https://api.dropbox.com/1/search/<root>/<path>"
	       #f
	       read-json)


; /shares     : https://www.dropbox.com/developers/reference/api#shares
; Description : Creates and returns a Dropbox link to files or folders users can
;               use to view a preview of the file in a web browser.
;
;               Note: Links created after April 23rd, 2012 no longer expire after
;               thirty days.
;
; Returns     : A Dropbox link to the given path. The link can be used publicly
;               and directs to a preview page of the file. For compatibility
;               reasons, it returns the link's expiration date in Dropbox's
;               usual date format. All links are currently set to expire far
;               enough in the future so that expiration is effectively not an
;               issue.
;
(define-method (shares root path #!key locale short_url)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api.dropbox.com/1/shares/<root>/<path>"))
	       #f
	       read-json)



; /media : https://www.dropbox.com/developers/reference/api#media
; Description : Returns a link directly to a file.
;
;               Similar to /shares. The difference is that this bypasses the
;               Dropbox webserver, used to provide a preview of the file, so
;               that you can effectively stream the contents of your media.
;
;        root : The root relative to which path is specified. Valid values are
;               sandbox and dropbox.
;        path : The path to the media file you want a direct link to.
;
;               The /media link expires after four hours, allotting enough time
;               to stream files, but not enough to leave a connection open
;               indefinitely.
;
; Returns     : A url that serves the media directly. Also returns the link's
;               expiration date in Dropbox's usual date format.
;
(define-method (media root path #!key locale)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api.dropbox.com/1/media/<root>/<path>"))
	       #f
	       read-json)



; /copy_ref   : https://www.dropbox.com/developers/reference/api#copy_ref
; Description : Creates and returns a copy_ref to a file. This reference string
;               can be used to copy that file to another user's Dropbox by
;               passing it in as the from_copy_ref parameter on /fileops/copy.
;
;        root : The root relative to which path is specified. Valid values are
;               sandbox and dropbox.
;        path : The path to the file you want a copy_ref to refer to.
;
; Returns     : A copy_ref to the specified file. For compatibility reasons, it
;               returns the link's expiration date in Dropbox's usual date
;               format. All links are currently set to expire far enough in the
;               future so that expiration is effectively not an issue.
;
(define-method (copy-ref root path)
	       "https://api.dropbox.com/1/copy_ref/<root>/<path>"
	       #f
	       read-json)



; /thumbnails : https://www.dropbox.com/developers/reference/api#thumbnails
; Description : Gets a thumbnail for an image. Note that this call goes to
;               api-content.dropbox.com instead of api.dropbox.com.
;
;        root : The root relative to which path is specified. Valid values are
;               sandbox and dropbox.
;        path : The path to the image file you want to thumbnail.
;
;               This method currently supports files with the following file
;               extensions: "jpg", "jpeg", "png", "tiff", "tif", "gif", and
;               "bmp".
;
;               Photos that are larger than 20MB in size will not be converted
;               to a thumbnail.
;
; Returns     : A thumbnail of the specified image's contents. The image
;               returned may be larger or smaller than the size requested,
;               depending on the size and aspect ratio of the original image.
;
;               The HTTP response contains the content metadata in JSON format
;               within an x-dropbox-metadata header.
;
(define-method (thumbnails root path #!key format size)
	       "https://api-content.dropbox.com/1/thumbnails/<root>/<path>"
	       #f
	       read-json)


; /chunked_upload : https://www.dropbox.com/developers/reference/api#chunked-upload
; Description     : Uploads large files to Dropbox in mulitple chunks. Also has
;                   the ability to resume if the upload is interrupted. This
;                   allows for uploads larger than the /files and /files_put
;                   maximum of 150 MB.
;
;                   Typical usage:
;
;                   1. Send a PUT request to /chunked_upload with the first
;                   chunk of the file without setting upload_id, and receive an
;                   upload_id in return.
;
;                   2. Repeatedly PUT subsequent chunks using the upload_id to
;                   identify the upload in progress and an offset representing
;                   the number of bytes transferred so far.
;
;                   3. After each chunk has been uploaded, the server returns a
;                   new offset representing the total amount transferred.
;
;                   4. After the last chunk, POST to /commit_chunked_upload to
;                   complete the upload.
;
;                   Chunks can be any size up to 150 MB. A typical chunk is
;                   4 MB. Using large chunks will mean fewer calls to
;                   /chunked_upload and faster overall throughput. However,
;                   whenever a transfer is interrupted, you will have to resume
;                   at the beginning of the last chunk, so it is often safer to
;                   use smaller chunks.
;
;                   If the offset you submit does not match the expected offset
;                   on the server, the server will ignore the request and
;                   respond with a 400 error that includes the current offset.
;                   To resume upload, seek to the correct offset (in bytes)
;                   within the file and then resume uploading from that point.
;
;                   A chunked upload can take a maximum of 24 hours before
;                   expiring.
;
; Returns         : JSON
;
(define-method (chunked_upload #!key upload_id offset)
	       (make-request method: 'PUT
			     uri: (uri-reference "https://api-content.dropbox.com/1/chunked_upload?param=val"))
	       #f;<>
	       read-json)



; /commit_chunked_upload : https://www.dropbox.com/developers/reference/api#commit-chunked-upload
; Description     : Completes an upload initiated by the /chunked_upload method.
;                   Saves a file uploaded via /chunked_upload to a user's
;                   Dropbox.
;
;                   /commit_chunked_upload is similar to /files_put. The main
;                   difference is that while /files_put takes the file contents
;                   in the request body, /commit_chunked_upload takes a
;                   parameter upload_id, which is obtained when the file
;                   contents are uploaded via /chunked_upload.
;
;                   Note that this call goes to api-content.dropbox.com instead
;                   of api.dropbox.com.
;
;            root : The root relative to which path is specified. Valid values
;                   are sandbox and dropbox.
;            path : The full path to the file you want to write to. This
;                   parameter should not point to a folder.
;
; Returns         : The metadata for the uploaded file.
;
(define-method (commit_chunked_upload root path #!key locale overwrite parent_rev upload_id)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api-content.dropbox.com/1/commit_chunked_upload/<root>/<path>"))
	       #f
	       read-json)



; /fileops/copy : https://www.dropbox.com/developers/reference/api#fileops-copy
; Description   : Copies a file or folder to a new location.
;
; Returns       : Metadata for the copy of the file or folder.
;
(define-method (fileops/copy #!key root from_path to_path locale from_copy_ref)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api.dropbox.com/1/fileops/copy"))
	       #f
	       read-json)



; /fileops/create_foler : https://www.dropbox.com/developers/reference/api#fileops-create-folder
; Description           : Creates a folder.
;
; Returns               : Metadata for the new folder.
;
(define-method (fileops/create_folder #!key root path locale)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api.dropbox.com/1/fileops/create_folder"))
	       #f
	       read-json)



; /fileops/delete : https://www.dropbox.com/developers/reference/api#fileops-delete
; Description     : Deletes a file or folder.
;
; Returns         : Metadata for the deleted file or folder.
;
(define-method (fileops/delete params root path locale)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api.dropbox.com/1/fileops/delete"))
	       #f
	       read-json)



; /fileops/move : https://www.dropbox.com/developers/reference/api#fileops-move
; Description   : Moves a file or folder to a new location.
;
; Returns       : Metadata for the moved file or folder.
;
(define-method (fileops/move #!key root from_path to_path locale)
	       (make-request method: 'POST
			     uri: (uri-reference "https://api.dropbox.com/1/fileops/move"))
	       #f
	       read-json)


)

